INSERT INTO PLATFORM VALUES ('XB1', 'XBOX One', 'Microsoft');
INSERT INTO PLATFORM VALUES ('XBOX', 'XBOX', 'Microsoft');
INSERT INTO PLATFORM VALUES ('X360', 'XBOX  360', 'Microsoft');
INSERT INTO PLATFORM VALUES ('PSX', 'PlayStation 1', 'Sony');
INSERT INTO PLATFORM VALUES ('PS2', 'PlayStation 2', 'Sony');
INSERT INTO PLATFORM VALUES ('PS3', 'PlayStation 3', 'Sony');
INSERT INTO PLATFORM VALUES ('PS4', 'PlayStation 4', 'Sony');

INSERT INTO PLATFORM VALUES ('NES', 'Nintendo Entertainment System', 'Nintendo');
INSERT INTO PLATFORM VALUES ('SNES', 'Super Nintendo Entertainment System', 'Nintendo');
INSERT INTO PLATFORM VALUES ('N64', 'Nintendo 64', 'Nintendo');
INSERT INTO PLATFORM VALUES ('GCN', 'Nintendo GameCube', 'Nintendo');
INSERT INTO PLATFORM VALUES ('WII', 'Wii', 'Nintendo');
INSERT INTO PLATFORM VALUES ('WII U', 'Wii-U', 'Nintendo');
INSERT INTO PLATFORM VALUES ('NS', 'Switch', 'Nintendo');
INSERT INTO PLATFORM VALUES ('VB', 'VirtualBoy', 'Nintendo');
INSERT INTO PLATFORM VALUES ('GB', 'GameBoy', 'Nintendo');
INSERT INTO PLATFORM VALUES ('GBC', 'GameBoy Color', 'Nintendo');
INSERT INTO PLATFORM VALUES ('GBA', 'GameBoy Advance', 'Nintendo');

INSERT INTO PLATFORM VALUES ('SG-1000', 'SG-1000', 'Sega');
INSERT INTO PLATFORM VALUES ('MS', 'Master System', 'Sega');
INSERT INTO PLATFORM VALUES ('MD', 'MegaDrive', 'Sega');
INSERT INTO PLATFORM VALUES ('SAT', 'Saturn', 'Sega');
INSERT INTO PLATFORM VALUES ('DC', 'Dreamcast', 'Sega');
INSERT INTO PLATFORM VALUES ('2600', '2600', 'Atari');
INSERT INTO PLATFORM VALUES ('5200', '5200', 'Atari');
INSERT INTO PLATFORM VALUES ('7800', '7800', 'Atari');
INSERT INTO PLATFORM VALUES ('Jaguar', 'Jaguar', 'Atari');
