DROP TABLE SALE;
DROP TABLE CLIENT;
DROP TABLE VIDEOGAMECOPY;
DROP TABLE VIDEOGAME;
DROP TABLE PLATFORM;
CREATE TABLE PLATFORM(
    acronym VARCHAR2(10) PRIMARY KEY,
    name VARCHAR2(20),
    company VARCHAR2(20)
);
CREATE TABLE VIDEOGAME(
    game_id VARCHAR2(20) PRIMARY KEY,
    name VARCHAR2(30),
    summary VARCHAR2(200),
    publisher VARCHAR2(30),
    year NUMBER(4),
    price FLOAT(5),
    platform_acronym VARCHAR2(10),
    FOREIGN KEY (platform_acronym)
        REFERENCES PLATFORM(acronym)
        ON DELETE SET NULL
);
CREATE TABLE VIDEOGAMECOPY(
    internal_id VARCHAR2(20) PRIMARY KEY,
    game_id VARCHAR2(10),
    FOREIGN KEY (game_id)
        REFERENCES VIDEOGAME(game_id)
        ON DELETE CASCADE
);
CREATE TABLE CLIENT(
    key VARCHAR2(64) PRIMARY KEY, /*sha-512*/
    username VARCHAR2(20),
    password VARCHAR2(20),
    nie VARCHAR2(9),
    name VARCHAR2(30),
    surname VARCHAR2(50),
    phone VARCHAR2(12),
    email VARCHAR2(200)
);
CREATE TABLE SALE(
    sale_id VARCHAR2(10) PRIMARY KEY,
    sale_client_key VARCHAR2(64),
    sale_copy_id VARCHAR2(20),
    sale_date date,
    sale_address VARCHAR2(100),
    FOREIGN KEY (sale_client_key)
        REFERENCES CLIENT(key),
    FOREIGN KEY (sale_copy_id)
        REFERENCES VIDEOGAMECOPY(internal_id)
);