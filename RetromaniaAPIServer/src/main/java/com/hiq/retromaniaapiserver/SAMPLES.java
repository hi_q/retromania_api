package com.hiq.retromaniaapiserver;

import com.hiq.retromaniaapiserver.model.*;
import com.hiq.retromaniaapiserver.utils.JacksonConverter;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SAMPLES {
    public static void executePrintsAndExports() throws IOException, ParserConfigurationException, SAXException {
        HashMap<String, Platform> platforms = new HashMap<>();
        platforms.put("PS2",new Platform("PS2","PlayStation 2","Sony"));

        Videogame videogame = new Videogame("SLES_54321","Need For Speed Carbon","Game of running through canyon","Electronic Arts",(short)2004,60.0f);

        videogame.setPlatform(platforms.get("PS2"));

        ArrayList<VideogameCopy> copies = new ArrayList<>();
        copies.add(new VideogameCopy("SLES_54321_001",videogame));
        copies.add(new VideogameCopy("SLES_54321_002",videogame));
        copies.add(new VideogameCopy("SLES_54321_003",videogame));
        copies.add(new VideogameCopy("SLES_54321_004",videogame));

        videogame.setCopies(copies);

        System.out.println(videogame);


        JacksonConverter<Videogame> videogameJacksonConverter = new JacksonConverter<>(Videogame.class);
        videogameJacksonConverter.toXmlFile(videogame,"jackson_examples/carbono.xml");
        videogameJacksonConverter.toJsonFile(videogame,"jackson_examples/carbono.json");

        Document document = videogameJacksonConverter.toXmlDocument(videogame);

        Client client = new Client(
                "2146545ads6ad",
                "X6016019R",
                "TheLuciano354",
                "averLaVidaLoca",
                "Yamil Luciano",
                "Castro D'Agostino",
                "+34681105195",
                "theluciano354@gmail.com");

        System.out.println(client);

        JacksonConverter<Client> clientJacksonConverter = new JacksonConverter<>(Client.class);
        clientJacksonConverter.toXmlFile(client,"jackson_examples/luciano_client.xml");
        clientJacksonConverter.toJsonFile(client,"jackson_examples/luciano_client.json");

        Sale sale = new Sale(
                "S_001",
                new Date(System.currentTimeMillis()),
                "C/ Real nº24",
                client,
                copies.get(0)
        );

        System.out.println(sale);

        JacksonConverter<Sale> saleJacksonConverter = new JacksonConverter<>(Sale.class);
        saleJacksonConverter.toXmlFile(sale,"jackson_examples/sale_luciano_carbon.xml");
        saleJacksonConverter.toJsonFile(sale,"jackson_examples/sale_luciano_carbon.json");
    }
}
