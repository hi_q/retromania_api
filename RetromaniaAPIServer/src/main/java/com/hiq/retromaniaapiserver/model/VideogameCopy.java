package com.hiq.retromaniaapiserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiserver.utils.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
@Entity
@Table(name = "VIDEOGAMECOPY")
@XmlRootElement(name = "copy")
@NamedQueries({
        @NamedQuery(name = "Videogamecopy.findAll", query = "SELECT v FROM VideogameCopy v"),
        @NamedQuery(name = "Videogamecopy.findByInternalID", query = "SELECT v FROM VideogameCopy v WHERE v.internalID = :internalID")})
@JsonRootName("videogameCopy")
@JacksonXmlRootElement(localName = "videogameCopy")
public class VideogameCopy implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "INTERNAL_ID")
    @JsonProperty("internalID")
    @JacksonXmlProperty(isAttribute = true,localName = "internalID")
    private String internalID;
    @JoinColumn(name = "GAME_ID", referencedColumnName = "GAME_ID")
    @ManyToOne
    @JsonIgnore
    private Videogame videogame;


    @OneToMany(mappedBy = "copy")
    @JsonIgnore
    private List<Sale> saleList;

    public VideogameCopy(){

    }
    public VideogameCopy(String internalID){
        this.internalID = internalID;
    }
    public VideogameCopy(String internalID, Videogame videogame){
        this.internalID = internalID;
        this.videogame = videogame;
    }

    public String getInternalID() {
        return internalID;
    }
    public void setInternalID(String internalID) {
        this.internalID = internalID;
    }

    public Videogame getVideogame() {
        return videogame;
    }
    public void setVideogame(Videogame videogame) {
        this.videogame = videogame;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(
                this.getClass().getSimpleName(),
                internalID
        ).toString();
    }
}
