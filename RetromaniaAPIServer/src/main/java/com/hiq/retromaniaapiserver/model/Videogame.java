package com.hiq.retromaniaapiserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiserver.utils.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "VIDEOGAME")
@XmlRootElement(name = "videogame")
@NamedQueries({
        @NamedQuery(name = "Videogame.findAll", query = "SELECT v FROM Videogame v"),
        @NamedQuery(name = "Videogame.findById", query = "SELECT v FROM Videogame v WHERE v.ID = :id"),
        @NamedQuery(name = "Videogame.findByName", query = "SELECT v FROM Videogame v WHERE v.name = ?1"),
        @NamedQuery(name = "Videogame.findBySummary", query = "SELECT v FROM Videogame v WHERE v.summary = :summary"),
        @NamedQuery(name = "Videogame.findByPublisher", query = "SELECT v FROM Videogame v WHERE v.publisher = ?1"),
        @NamedQuery(name = "Videogame.findByYear", query = "SELECT v FROM Videogame v WHERE v.year = :year"),
        @NamedQuery(name = "Videogame.findByPrice", query = "SELECT v FROM Videogame v WHERE v.price = :price")})
@JsonRootName("videogame")
@JacksonXmlRootElement(localName = "videogame")
public class Videogame implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "GAME_ID")
    @JsonProperty("id")
    @JacksonXmlProperty(isAttribute = true,localName = "id")
    private String ID;
    @Size(max = 30)
    @Column(name = "NAME")
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @Size(max = 200)
    @Column(name = "SUMMARY")
    @JsonProperty("summary")
    @JacksonXmlProperty(localName = "summary")
    private String summary;
    @Size(max = 30)
    @Column(name = "PUBLISHER")
    @JsonProperty("publisher")
    @JacksonXmlProperty(localName = "publisher")
    private String publisher;
    @JsonProperty("year")
    @JacksonXmlProperty(localName = "year")
    private Short year;// @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRICE")
    @JsonProperty("price")
    @JacksonXmlProperty(localName = "price")
    private Float price;

    @JsonProperty("platform")
    @JacksonXmlProperty(localName = "platform")
    @JoinColumn(name = "PLATFORM_ACRONYM", referencedColumnName = "ACRONYM")
    @ManyToOne
    private Platform platform;

    @OneToMany(mappedBy = "videogame")
    @JsonProperty("copies")
    @JacksonXmlProperty(localName = "copies")
    private List<VideogameCopy> copies;

    protected Videogame() {

    }
    public Videogame(String ID, String name, String summary, String publisher, Short year, Float price) {
        this.ID = ID;
        this.name = name;
        this.summary = summary;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
    }
    public Videogame(String ID, String name, String summary, String publisher, Short year, Float price, Platform platform, ArrayList<VideogameCopy> copies) {
        this.ID = ID;
        this.name = name;
        this.summary = summary;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
        this.platform = platform;
        this.copies = copies;
    }

    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public Float getPrice() {
        return price;
    }
    public void setPrice(Float price) {
        this.price = price;
    }
    public String getPublisher() {
        return publisher;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public Short getYear() {
        return year;
    }
    public void setYear(Short year) {
        this.year = year;
    }

    public Platform getPlatform() {
        return platform;
    }
    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public List<VideogameCopy> getCopies() {
        return copies;
    }
    public void setCopies(ArrayList<VideogameCopy> copies) {
        this.copies = copies;
    }

    @Override
    public String toString() {
        String platformString =
                platform.toString().replace("\n","\n\t");
        String copiesString = "";
        if (copies != null) {
        StringBuilder copiesStringBuild = new StringBuilder("VideogameCopies:\n\t\t");
            for (VideogameCopy copy :
                    copies) {
                copiesStringBuild.append(
                        copy.toString().replace("\n", "\n\t\t"));
                copiesStringBuild.append("\n\t\t");
            }
        copiesString = copiesStringBuild.toString();
        }

        return new ToStringBuilder
                (getClass().getSimpleName(),
            ID,
                    name,
                    summary,
                    publisher,
                    year.toString(),
                        platformString,
                        copiesString)

                .toString();
    }
}
