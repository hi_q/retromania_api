/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.hiq.retromaniaapiserver.utils.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "PLATFORM")
@XmlRootElement(name = "platform")
@NamedQueries({
    @NamedQuery(name = "Platform.findAll", query = "SELECT p FROM Platform p"),
    @NamedQuery(name = "Platform.findByAcronym", query = "SELECT p FROM Platform p WHERE p.acronym = :acronym"),
    @NamedQuery(name = "Platform.findByName", query = "SELECT p FROM Platform p WHERE p.name = :name"),
    @NamedQuery(name = "Platform.findByCompany", query = "SELECT p FROM Platform p WHERE p.company = :company")})
public class Platform implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ACRONYM")
    @JsonProperty("acronym")
    @JacksonXmlProperty(localName = "acronym")
    private String acronym;
    @Size(max = 20)
    @Column(name = "NAME")
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @Size(max = 20)
    @Column(name = "COMPANY")
    @JsonProperty("company")
    @JacksonXmlProperty(localName = "company")
    private String company;

    public Platform() {
    }
    public Platform(String acronym) {
        this.acronym = acronym;
    }
    public Platform(String acronym, String name, String company) {
        this.acronym = acronym;
        this.name = name;
        this.company = company;
    }

    public String getAcronym() {
        return acronym;
    }
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (acronym != null ? acronym.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Platform)) {
            return false;
        }
        Platform other = (Platform) object;
        if ((this.acronym == null && other.acronym != null) || (this.acronym != null && !this.acronym.equals(other.acronym))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(
            this.getClass().getSimpleName(),
            acronym,
                name,
                company
        ).toString();
    }
    
}
