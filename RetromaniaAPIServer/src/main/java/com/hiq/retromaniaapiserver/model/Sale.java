package com.hiq.retromaniaapiserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiserver.utils.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
@Table(name = "SALE")
@XmlRootElement(name = "sale")
@NamedQueries({
        @NamedQuery(name = "Sale.findAll", query = "SELECT s FROM Sale s"),
        @NamedQuery(name = "Sale.findBySaleId", query = "SELECT s FROM Sale s WHERE s.id = :id"),
        @NamedQuery(name = "Sale.findBySaleDate", query = "SELECT s FROM Sale s WHERE s.date = :date"),
        @NamedQuery(name = "Sale.findBySaleAddress", query = "SELECT s FROM Sale s WHERE s.address = :address")})
@JsonPropertyOrder({"id","date","client","copies"})
@JsonRootName("sale")
@JacksonXmlRootElement(localName = "sale")
public class Sale implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "SALE_ID")
    @JsonProperty("id")
    @JacksonXmlProperty(isAttribute = true,localName = "id")
    private String id;
    @Column(name = "SALE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 100)
    @Column(name = "SALE_ADDRESS")
    @JsonProperty("address")
    @JacksonXmlProperty(localName = "address")
    private String address;
    @JoinColumn(name = "SALE_CLIENT_KEY", referencedColumnName = "KEY")
    @ManyToOne
    @JsonProperty("client")
    @JacksonXmlProperty(localName = "client")
    private Client client;
    @JoinColumn(name = "SALE_COPY_ID", referencedColumnName = "INTERNAL_ID")
    @ManyToOne
    @JsonProperty("copy")
    @JacksonXmlProperty(localName = "copy")
    private VideogameCopy copy;

    protected Sale() {
    }
    public Sale(String id, Date date, String address, Client client, VideogameCopy copy) {
        this.id = id;
        this.date = date;
        this.address = address;
        this.client = client;
        this.copy = copy;
    }

    @JsonIgnore
    @Transient
    private SimpleDateFormat defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat getDefaultDateFormat(){
        return defaultDateFormat;
    }
    private void setDefaultDateFormat(SimpleDateFormat simpleDateFormat){
        this.defaultDateFormat = simpleDateFormat;
    }

    @JsonIgnore
    public Date getDate() {
        return date;
    }
    @JsonProperty(value = "date")
    @JacksonXmlProperty(localName = "date")
    @Transient
    public String getFormattedDate(){
        return getDefaultDateFormat().format(getDate());
    }
    @JsonIgnore
    public void setDate(Date date) {
        this.date = date;
    }
    @Transient
    public void setFormattedDate(String date) throws ParseException {
        this.date = getDefaultDateFormat().parse(date);
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public VideogameCopy getCopy() {
        return copy;
    }
    public void setCopy(VideogameCopy copy) {
        this.copy = copy;
    }

    @Override
    public String toString() {
        String clientString =
                client.toString().replace("\n","\n\t");
        String copiesString = copy.toString();
        String dateString =
                getFormattedDate();

        return new ToStringBuilder(
                getClass().getSimpleName(),
                id,
                dateString,
                address,
                clientString,
                copiesString
        ).toString();
    }
}
