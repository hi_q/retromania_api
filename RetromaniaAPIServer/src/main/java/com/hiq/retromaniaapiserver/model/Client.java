package com.hiq.retromaniaapiserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiserver.utils.ToStringBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "CLIENT")
@XmlRootElement(name = "client")
@NamedQueries({
        @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
        @NamedQuery(name = "Client.findByKey", query = "SELECT c FROM Client c WHERE c.key = :key"),
        @NamedQuery(name = "Client.findByUsername", query = "SELECT c FROM Client c WHERE lower(c.username) = lower(?1)"),
        @NamedQuery(name = "Client.findByPassword", query = "SELECT c FROM Client c WHERE c.password = :password"),
        @NamedQuery(name = "Client.findByNIE", query = "SELECT c FROM Client c WHERE c.NIE = :nie"),
        @NamedQuery(name = "Client.findByName", query = "SELECT c FROM Client c WHERE c.name = :name"),
        @NamedQuery(name = "Client.findBySurname", query = "SELECT c FROM Client c WHERE c.surname = :surname"),
        @NamedQuery(name = "Client.findByPhone", query = "SELECT c FROM Client c WHERE c.phone = :phone"),
        @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email")})
@JsonRootName("client")
@JacksonXmlRootElement(localName = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "KEY")
    @JsonProperty("key")
    @JacksonXmlProperty(localName = "key",isAttribute = true)
    private String key;
    @Size(max = 9)
    @Column(name = "NIE", unique = true, nullable = false)
    private String NIE;
    @Size(max = 20)
    @Column(name = "USERNAME")
    @JsonProperty("username")
    @JacksonXmlProperty(localName = "username")
    private String username;
    @Size(max = 20)
    @Column(name = "PASSWORD")
    @JsonProperty("password")
    @JacksonXmlProperty(localName = "password")
    private String password;
    @Size(max = 30)
    @Column(name = "NAME")
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "SURNAME")
    @JsonProperty("surname")
    @JacksonXmlProperty(localName = "surname")
    private String surname;
    //@Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 12)
    @Column(name = "PHONE")
    @JsonProperty("phone")
    @JacksonXmlProperty(localName = "phone")
    private String phone;
    //@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    /* és larga pero permite detectar que un email es valido */
    @Size(max = 200)
    @Column(name = "EMAIL")
    @JsonProperty("email")
    @JacksonXmlProperty(localName = "email")
    private String email;

    protected Client() {
    }
    public Client(String key, String NIE, String username, String password) {
        this.key = key;
        this.NIE = NIE;
        this.username = username;
        this.password = password;
    }
    public Client(String key, String NIE, String username, String password, String name, String surname, String phone, String email) {
        this.key = key;
        this.NIE = NIE;
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getNIE() {
        return NIE;
    }
    public void setNIE(String NIE) {
        this.NIE = NIE;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(getClass().getSimpleName(),
                key,
                NIE,
                username,
                password,
                name,
                surname,
                phone,
                email)

                .toString();
    }
}
