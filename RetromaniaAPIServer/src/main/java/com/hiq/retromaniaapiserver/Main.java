/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 *
 * @author Luciano
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Main {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, URISyntaxException {
        SpringApplication.run(Main.class, args);
    }
}
