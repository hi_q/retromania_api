package com.hiq.retromaniaapiserver.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * Clase genérica que hace uso de la librería Jackson para la importación y exportación de cualquier
 * objeto de clase que utilice anotaciones Jackson o que pueda ser leído correctamente.
 *
 * Las EXCEPCIONES DE LECTURA son expulsadas y trasladadas al usuario, ya que esta clase no tiene
 * intención de tratarlas sinó de servir como vía rápida de acceso.
 *
 * @author TheLuciano354
 * @param <T> Representa cualquier tipo de clase Serializable que se pretenda exportar o leer usando Jackson.
 */
public class JacksonConverter<T>{

    /**
     * Obligatorio para invocar este método.<br/>
     * Jackson necesita hacer uso constante de la información de la clase.
     *
     * Se proporciona únicamente en el constructor y debe coincidir con el tipo<br/>
     * con el cual se invoca esta clase.
     *
     * @see #JacksonConverter(Class) Constructor que debe recibir los parámetros
     *
     */
    private Class<T> type;

    /**
     * Constructor que recibe los parámetros de tipo de objeto (Class) <br/>
     * para iniciar el convertidor de objeto con el tipo deseado.
     *
     * @param type Tipo de objeto. Debe coincidir OBLIGATORIAMENTE con el tipo de objeto de la instancia.
     */
    public JacksonConverter(Class<T> type) {
        this.type = type;
    }

    /**
     * Encapsulamiento PRIVADO de ObjectMapper de jackson.
     * Este encapsulamiento crea una dependencia entre los métodos de la clase
     * y este mismo, pudiendose realizar cambios cruciales del mapper en poco tiempo.
     *
     * @return Nuevo ObjectMapper con la configuración predeterminada.
     */
    private ObjectMapper getDefaultMapper(){
        return new ObjectMapper();
    }
    /**
     * Equivalente de getObjectMapper para lectura y escritura XML.
     *
     * @see ObjectMapper Método equivalente para obtener el mapper JSON.
     * @return Nuevo XmlMapper con la configuración predeterminada.
     */
    private XmlMapper getDefaultXmlMapper(){
        return new XmlMapper();
    }

    /**
     * Método para serializar un objeto en un BYTE ARRAY. Formato JSON.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson, y la clase de la
     * cual forma parte debe llevar las anotaciones Jackson correspondientes
     * para el formato de salida y entrada JSON.
     *
     * @param toParse Objeto del tipo invocado.
     * @return Array de bytes del objeto serializado
     * @throws IOException Error de escritura de objeto
     */
    public byte[] toJson(T toParse) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        getDefaultMapper().writerWithDefaultPrettyPrinter().writeValue(outputStream,toParse);
        return outputStream.toByteArray();
    }
    /**
     * Método para serializar un objeto en un STRING. Formato JSON.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson, y la clase de la
     * cual forma parte debe llevar las anotaciones Jackson correspondientes
     * para el formato de salida y entrada JSON.
     *
     * @param toParse Objeto del tipo invocado.
     * @return String del objeto serializado
     * @throws IOException Error de escritura de String
     */
    public String toJsonString(T toParse) throws JsonProcessingException {
        return getDefaultMapper().
                writerWithDefaultPrettyPrinter().
                writeValueAsString(toParse);
    }
    /**
     * Método para serializar un objeto en un ARCHIVO. Formato JSON.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson, y la clase de la
     * cual forma parte debe llevar las anotaciones Jackson correspondientes
     * para el formato de salida y entrada JSON.
     *
     * @param toParse Objeto del tipo invocado.
     * @param fileOutput Objeto de tipo File previamente construido para depositar el contenido.
     * @throws IOException Error de escritura de archivo
     */
    public void toJsonFile(T toParse, File fileOutput) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        getDefaultMapper().writerWithDefaultPrettyPrinter().writeValue(fileOutput,toParse);
    }
    /**
     * Método para serializar un objeto en un ARCHIVO. Formato JSON.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson, y la clase de la
     * cual forma parte debe llevar las anotaciones Jackson correspondientes
     * para el formato de salida y entrada JSON.
     *
     * @param toParse Objeto del tipo invocado.
     * @param fileOutputPath Ruta de archivo para construir un objeto File para depositar el contenido.
     * @throws IOException Error de escritura de archivo
     */
    public void toJsonFile(T toParse, String fileOutputPath) throws IOException {
        toJsonFile(toParse,new File(fileOutputPath));
    }
    /**
     * Método para serializar un objeto en un objeto JsonObject. Formato JSON.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson, y la clase de la
     * cual forma parte debe llevar las anotaciones Jackson correspondientes
     * para el formato de salida y entrada JSON.
     *
     * @see JSONObject Estándar para el procesamiento de JSON
     * @param toParse Objeto del tipo invocado.
     * @throws IOException Error de creación de JSONObject
     */
    public JSONObject toJsonObject(T toParse) throws IOException {
        String resultAsString = toJsonString(toParse);
        return new JSONObject(resultAsString);
    }

    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un BYTE ARRAY. Formato JSON.
     * La estructura debe cumplir con las especificaciones JSON para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param toRead Array de bytes a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromJson(byte[] toRead) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(toRead);
        return getDefaultMapper().readValue(inputStream,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un STRING. Formato JSON.
     * La estructura debe cumplir con las especificaciones JSON para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param toRead Objeto String a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromJsonString(String toRead) throws IOException {
        return getDefaultMapper().readValue(toRead,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un ARCHIVO. Formato JSON.
     * La estructura debe cumplir con las especificaciones JSON para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param fileInput Objeto File que indica el archivo que se va a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromJsonFile(File fileInput) throws IOException {
        return getDefaultMapper().readValue(fileInput,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un ARCHIVO. Formato JSON.
     * La estructura debe cumplir con las especificaciones JSON para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param fileInputPath Ruta para construir el objeto File que indica el archivo que se va a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromJsonFile(String fileInputPath) throws IOException {
        return fromJsonFile(new File(fileInputPath));
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un JSON OBJECT. Formato JSON.
     * La estructura debe cumplir con las especificaciones JSON para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param toRead Objeto JSONObject a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromJsonObject(JSONObject toRead) throws IOException{
        return getDefaultMapper().readValue(toRead.toString(),type);
    }

    /**
     * Método para serializar un objeto en un BYTE ARRAY. Formato XML.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson Dataformat XML,
     * y la clase de la cual forma parte debe llevar las anotaciones
     * Jackson correspondientes para el formato de salida y entrada XML.
     *
     * @param toParse Objeto del tipo invocado que se va a serializar.
     * @return Array de bytes del objeto serializado en formato XML.
     * @throws IOException Error de escritura de objeto
     */
    public byte[] toXml(T toParse) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        getDefaultXmlMapper().
                writerWithDefaultPrettyPrinter().
                writeValue(outputStream,toParse);
        return outputStream.toByteArray();
    }
    /**
     * Método para serializar un objeto en un STRING. Formato XML.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson Dataformat XML,
     * y la clase de la cual forma parte debe llevar las anotaciones
     * Jackson correspondientes para el formato de salida y entrada XML.
     *
     * @param toParse Objeto del tipo invocado que se va a serializar.
     * @return String del objeto serializado en formato XML.
     * @throws IOException Error de escritura de objeto
     */
    public String toXmlString(T toParse) throws JsonProcessingException {
        return getDefaultXmlMapper().
                writerWithDefaultPrettyPrinter().
                writeValueAsString(toParse);
    }
    /**
     * Método para serializar un objeto en un ARCHIVO. Formato XML.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson Dataformat XML,
     * y la clase de la cual forma parte debe llevar las anotaciones
     * Jackson correspondientes para el formato de salida y entrada XML.
     *
     * @param toParse Objeto del tipo invocado que se va a serializar.
     * @param fileOutput Objeto de tipo File previamente construido para depositar el contenido.
     * @throws IOException Error de escritura de objeto
     */
    public void toXmlFile(T toParse, File fileOutput) throws IOException {
        XmlMapper mapper = getDefaultXmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION,true);
        mapper.
            writerWithDefaultPrettyPrinter().
            writeValue(fileOutput,toParse);
    }
    /**
     * Método para serializar un objeto en un ARCHIVO. Formato XML.
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson Dataformat XML,
     * y la clase de la cual forma parte debe llevar las anotaciones
     * Jackson correspondientes para el formato de salida y entrada XML.
     *
     * @param toParse Objeto del tipo invocado que se va a serializar.
     * @param fileOutputPath Ruta de archivo para construir un objeto File para depositar el contenido.
     * @throws IOException Error de escritura de objeto
     */
    public void toXmlFile(T toParse, String fileOutputPath) throws IOException{
        toXmlFile(toParse,new File(fileOutputPath));
    }
    /**
     * Método para serializar un objeto en un OBJETO DOCUMENT. Formato XML.
     *
     * Document es el estándar del World Wide Web Consortium para modificar
     * y gestionar estructuras XML con el estándar Document Object Model (DOM).
     *
     * Debe ser del tipo invocado en esta clase, debe ser Serializable
     * y cumplir con las especificaciones de Jackson Dataformat XML,
     * y la clase de la cual forma parte debe llevar las anotaciones
     * Jackson correspondientes para el formato de salida y entrada XML.
     *
     * @see org.w3c.dom.Document Tipo de objeto estandarizado por W3C que se retorna.
     * @param toParse Objeto del tipo invocado que se va a serializar.
     * @return Objeto de tipo Document que contiene los datos en formato XML.
     * @throws IOException Error de escritura de objeto
     */
    public Document toXmlDocument(T toParse) throws ParserConfigurationException, IOException, SAXException {
        XmlMapper mapper = getDefaultXmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION,true);
        String res = mapper.writerWithDefaultPrettyPrinter().
                writeValueAsString(toParse);
        InputSource is = new InputSource(); // input source is only way to parse xml
        is.setCharacterStream(
        new StringReader( // string reader puts result of string xml
                mapper.writerWithDefaultPrettyPrinter().
                writeValueAsString(toParse)));
        return DocumentBuilderFactory.newInstance().newDocumentBuilder().
                parse(is);
    }


    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un BYTE ARRAY. Formato XML.
     * La estructura debe cumplir con las especificaciones XML para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param toRead Array de bytes a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromXml(byte[] toRead) throws IOException{
        ByteArrayInputStream inputStream = new ByteArrayInputStream(toRead);
        return getDefaultXmlMapper().readValue(inputStream,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un String. Formato XML.
     * La estructura debe cumplir con las especificaciones XML para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param toRead String a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromXmlString(String toRead) throws IOException{
        return getDefaultXmlMapper().readValue(toRead,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un ARCHIVO. Formato XML.
     * La estructura debe cumplir con las especificaciones XML para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param fileInput Objeto file que apunta al archivo que debe ser leído
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromXmlFile(File fileInput) throws IOException{
        return getDefaultXmlMapper().readValue(fileInput,type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un ARCHIVO. Formato XML.
     * La estructura debe cumplir con las especificaciones XML para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * @param fileInputPath Ruta de archivo que construirá el objeto file que apuntará al archivo que debe ser leído
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromXmlFile(String fileInputPath) throws IOException{
        return getDefaultXmlMapper().readValue(new File(fileInputPath),type);
    }
    /**
     * Método para deserializar un objeto en el tipo de objeto que invoca la clase,
     * leído desde un DOM DOCUMENT. Formato XML.
     * La estructura debe cumplir con las especificaciones XML para poder
     * ser deconstruído sin errores de ningún tipo.
     * Cualquier excepción será expulsada.
     *
     * Document es el objeto de estándar DOM (Document Object Model) para
     * el guardado de objetos XML desarrollado por el World Wide Web Consortium.
     *
     * @see org.w3c.dom.Document Objeto estándar del w3c para guardar documentos XML
     * @param toRead Objeto tipo Document a procesar
     * @return Objeto resultante de la lectura
     * @throws IOException Error de lectura de objeto
     */
    public T fromXmlDocument(Document toRead) throws IOException, TransformerException {
        return fromXmlString(documentToString(toRead));
    }

    private Transformer getDefaultXmlTransformer() throws TransformerConfigurationException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        return transformer;
    }
    private String documentToString(Document document) throws TransformerException {
        StringWriter sw = new StringWriter();
        getDefaultXmlTransformer().transform(new DOMSource(document), new StreamResult(sw));
        return sw.toString();
    }
}
