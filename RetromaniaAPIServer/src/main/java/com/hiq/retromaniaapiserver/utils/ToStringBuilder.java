package com.hiq.retromaniaapiserver.utils;

/**
 * Clase que sirve de utilidad de desarrollo para el proyecto actual.
 * Mediante una única invocación permite construir un método ToString
 * de forma rápida con infinidad de argumentos.
 *
 * @see #ToStringBuilder(String, String...) Único constructor.
 * @author TheLuciano354
 * @version 0.5
 */
public class ToStringBuilder {

    /**
     * Objeto que guarda los elementos clave a procesar en el método toString.
     * Solo pueden ser colocados mediante el constructor.
     *
     * @see #toString() Método que hace uso de estos items.
     */
    private String[] items;
    /**
     * Objeto que guarda el nombre de la clase cuyo toString hace referencia.
     * Solo pueden ser colocados mediante el constructor.
     *
     * @see #toString() Método que hace uso del className.
     */
    private String className;

    /**
     * Único constructor. Permite asignar los valores propios del toString que se va a generar.
     *
     * @param className Nombre de la clase de la cual se generará el toString.
     * @param items Argumentos ilimitados de tipo String para colocar después del className.
     */
    public ToStringBuilder(String className, String ... items) {
        this.items = items;
        this.className = className;
    }

    protected String controlOpen = ":\n\t";
    protected String controlSepparator = "\n\t";
    protected String controlClose = ";";
    private final static String SPACE = " ";

    public String getControlOpen() {
        return controlOpen;
    }
    public void setControlOpen(String controlOpen) {
        this.controlOpen = controlOpen;
    }
    public String getControlSepparator() {
        return controlSepparator;
    }
    public void setControlSepparator(String controlSepparator) {
        this.controlSepparator = controlSepparator;
    }
    public String getControlClose() {
        return controlClose;
    }
    public void setControlClose(String controlClose) {
        this.controlClose = controlClose;
    }

    /**
     * Método en el cual se basa esta clase. Retorna un toString generado automáticamente con los
     * carácteres de control.
     * @return String que representa los elementos colocados en el objeto.
     */
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(className)
                .append(controlOpen);

        for (int i = 0; i < items.length; i++) {
            builder.append(items[i]);
            if (i != items.length-1) {
                builder.append(controlSepparator);
            }
            else builder.append(controlClose);
        }

        return builder.toString();
    }
}
