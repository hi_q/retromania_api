package com.hiq.retromaniaapiserver.rest_skeleton.service;

import com.hiq.retromaniaapiserver.model.VideogameCopy;
import com.hiq.retromaniaapiserver.rest_skeleton.dao.VideogameCopyRepository;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.AbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IVideogameCopyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VideogameCopyService extends AbstractService<VideogameCopy,String> implements IVideogameCopyService {

    @Autowired
    protected VideogameCopyRepository repository;

    @Override
    public VideogameCopyRepository getRepository() {
        return repository;
    }
    public void setRepository(VideogameCopyRepository repository) {
        this.repository = repository;
    }

    @Override
    public void update(String primaryKey, VideogameCopy entity) {
        getRepository().save(
                new VideogameCopy(
                        primaryKey,
                        entity.getVideogame()));
    }

    @Override
    public VideogameCopy get(String id) {
        return getRepository().findById(id).get();
    }
}
