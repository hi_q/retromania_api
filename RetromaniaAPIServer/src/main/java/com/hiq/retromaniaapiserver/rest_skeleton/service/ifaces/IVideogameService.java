package com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces;

import com.hiq.retromaniaapiserver.model.Videogame;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IVideogameService extends IAbstractService<Videogame,String> {
    public List<Videogame> getVideogameByName(String name);
    public List<Videogame> getVideogameByPublisher(String publisher);
}
