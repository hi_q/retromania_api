package com.hiq.retromaniaapiserver.rest_skeleton.controller.base;

import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractRestController<E,PK>{
    public abstract IAbstractService<E,PK> getService();

    protected final static String primaryKey = "key";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<E> getAll() {
        return getService().getAll();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Boolean> add(@RequestBody E entity) {
        getService().add(entity);
        return new ResponseEntity<>(true, HttpStatus.CREATED);
    }
    @RequestMapping(value = "/{"+primaryKey+"}", method = RequestMethod.GET)
    public E get(@PathVariable(primaryKey) PK key) {
        System.out.println(key);
        return getService().get(key);
    }
    @RequestMapping(value = "/"+"{"+primaryKey+"}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> update(@RequestBody E entity, @PathVariable(primaryKey) PK key) {
        getService().update(key,entity);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
    @RequestMapping(value = "/"+"{"+primaryKey+"}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable(primaryKey) E entity) {
        getService().remove(entity);
        return new ResponseEntity<>(true, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "count", method = RequestMethod.GET)
    public long count(){
        return getService().count();
    }
}
