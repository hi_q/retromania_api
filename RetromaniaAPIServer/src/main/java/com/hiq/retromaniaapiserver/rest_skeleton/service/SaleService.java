package com.hiq.retromaniaapiserver.rest_skeleton.service;

import com.hiq.retromaniaapiserver.model.Sale;
import com.hiq.retromaniaapiserver.rest_skeleton.dao.SaleRepository;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.AbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.ISaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaleService extends AbstractService<Sale,String> implements ISaleService {

    @Autowired
    protected SaleRepository repository;

    @Override
    public SaleRepository getRepository() {
        return repository;
    }
    public void setRepository(SaleRepository repository) {
        this.repository = repository;
    }

    @Override
    public void update(String primaryKey, Sale entity) {
        getRepository().save(
                new Sale(
                        primaryKey,
                        entity.getDate(),
                        entity.getAddress(),
                        entity.getClient(),
                        entity.getCopy()));
        //(String id, Date date, String address, Client client, VideogameCopy copy)
    }

    @Override
    public Sale get(String id) {
        return getRepository().findById(id).get();
    }
}
