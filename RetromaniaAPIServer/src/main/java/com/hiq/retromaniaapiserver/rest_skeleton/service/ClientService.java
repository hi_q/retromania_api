package com.hiq.retromaniaapiserver.rest_skeleton.service;

import com.hiq.retromaniaapiserver.model.Client;
import com.hiq.retromaniaapiserver.rest_skeleton.dao.ClientRepository;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.AbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService extends AbstractService<Client,String> implements IClientService {

    @Autowired
    protected ClientRepository repository;

    @Override
    public ClientRepository getRepository() {
        return repository;
    }
    public void setRepository(ClientRepository repository) {
        this.repository = repository;
    }

    @Override
    public void update(String primaryKey, Client entity) {
        getRepository().save(new Client(primaryKey,entity.getNIE(),entity.getUsername(),entity.getPassword(),entity.getName(),entity.getSurname(),entity.getPhone(),entity.getEmail()));
        //String key, String NIE, String username, String password, String name, String surname, String phone, String email
    }

    @Override
    public Client get(String key) {
        return getRepository().findById(key).get();
    }

    @Override
    public List<Client> getClientByUsername(String username) {
        return getRepository().findByUsername(username);
    }
}
