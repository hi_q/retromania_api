package com.hiq.retromaniaapiserver.rest_skeleton.dao;

import com.hiq.retromaniaapiserver.model.Videogame;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface VideogameRepository extends CrudRepository<Videogame, String> {
    @Override
    List<Videogame> findAll();
    Optional<Videogame> findById(String id);
    List<Videogame> findByName(String name);
    List<Videogame> findByPublisher(String publisher);
}
