package com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces;

import com.hiq.retromaniaapiserver.model.Sale;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.stereotype.Component;

@Component
public interface ISaleService extends IAbstractService<Sale,String> {

}
