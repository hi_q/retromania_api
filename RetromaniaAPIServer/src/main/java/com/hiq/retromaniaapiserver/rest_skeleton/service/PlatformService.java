package com.hiq.retromaniaapiserver.rest_skeleton.service;

import com.hiq.retromaniaapiserver.model.Platform;
import com.hiq.retromaniaapiserver.rest_skeleton.dao.PlatformRepository;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.AbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IPlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlatformService extends AbstractService<Platform,String> implements IPlatformService {

    @Autowired
    protected PlatformRepository repository;

    @Override
    public PlatformRepository getRepository() {
        return repository;
    }
    public void setRepository(PlatformRepository repository) {
        this.repository = repository;
    }

    @Override
    public void update(String primaryKey, Platform entity) {
        getRepository().save(new Platform(primaryKey,entity.getName(),entity.getCompany()));
    }

    @Override
    public Platform get(String acronym) {
        return getRepository().findById(acronym).get();
    }
}
