package com.hiq.retromaniaapiserver.rest_skeleton.dao;

import com.hiq.retromaniaapiserver.model.Platform;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface PlatformRepository extends CrudRepository<Platform, String> {
    Optional<Platform> findById(String id);
    List<Platform> findByName(String name);
    List<Platform> findByCompany(String company);
}
