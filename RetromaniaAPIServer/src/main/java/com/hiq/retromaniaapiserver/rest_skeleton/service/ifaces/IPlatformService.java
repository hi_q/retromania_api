package com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces;

import com.hiq.retromaniaapiserver.model.Platform;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.stereotype.Component;

@Component
public interface IPlatformService extends IAbstractService<Platform,String> {

}
