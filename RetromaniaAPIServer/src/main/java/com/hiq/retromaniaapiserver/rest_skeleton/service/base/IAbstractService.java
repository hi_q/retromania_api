/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.service.base;

import java.util.List;


public interface IAbstractService<E,PK> {
    public List<E> getAll();
    public E get(PK primaryKey);
    public void add(E entity);
    public void update(PK primaryKey, E entity);
    public void remove(E entity);
    public long count();
}
