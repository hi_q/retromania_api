/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.controller;

import com.hiq.retromaniaapiserver.model.Sale;
import com.hiq.retromaniaapiserver.rest_skeleton.controller.base.AbstractRestController;
import com.hiq.retromaniaapiserver.rest_skeleton.service.SaleService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "sales")
public class SaleRestController extends AbstractRestController<Sale,String> {

    @Autowired
    private SaleService saleService;

    private final static String root = "sales";
    private final static String primaryKey = "id";
    
    @Override
    public IAbstractService<Sale, String> getService() {
        return saleService;
    }

}