/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.controller;

import com.hiq.retromaniaapiserver.model.Client;
import com.hiq.retromaniaapiserver.rest_skeleton.controller.base.AbstractRestController;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "clients")
public class ClientRestController extends AbstractRestController<Client,String> {

    @Autowired
    private IClientService clientService;

    private final static String root = "clients";
    private final static String primaryKey = "key";
    private final static String usernameKey = "username";

    @Override
    public IAbstractService<Client, String> getService() {
        return clientService;
    }


    @RequestMapping(value = "/"+ usernameKey +"/{"+ usernameKey +"}", method = RequestMethod.GET)
    public List<Client> getByName(@PathVariable(usernameKey) String username) {
        return clientService.getClientByUsername(username);
    }
}