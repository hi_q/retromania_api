package com.hiq.retromaniaapiserver.rest_skeleton.dao;

import com.hiq.retromaniaapiserver.model.VideogameCopy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface VideogameCopyRepository extends CrudRepository<VideogameCopy, String> {
    @Override
    List<VideogameCopy> findAll();
    Optional<VideogameCopy> findById(String internalID);
}
