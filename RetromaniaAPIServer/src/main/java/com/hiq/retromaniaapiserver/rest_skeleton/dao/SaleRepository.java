package com.hiq.retromaniaapiserver.rest_skeleton.dao;

import com.hiq.retromaniaapiserver.model.Sale;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface SaleRepository extends CrudRepository<Sale, String> {
    @Override
    List<Sale> findAll();
    Optional<Sale> findById(String id);
}
