/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.controller;

import com.hiq.retromaniaapiserver.model.VideogameCopy;
import com.hiq.retromaniaapiserver.rest_skeleton.controller.base.AbstractRestController;
import com.hiq.retromaniaapiserver.rest_skeleton.service.VideogameCopyService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "copies")
public class VideogameCopyRestController extends AbstractRestController<VideogameCopy,String> {

    @Autowired
    private VideogameCopyService videogameCopyService;

    private final static String root = "copies";
    private final static String primaryKey = "internalID";
    
    @Override
    public IAbstractService<VideogameCopy, String> getService() {
        return videogameCopyService;
    }
}