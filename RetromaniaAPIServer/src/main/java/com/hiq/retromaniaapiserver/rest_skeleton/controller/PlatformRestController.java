/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.controller;

import com.hiq.retromaniaapiserver.model.Platform;
import com.hiq.retromaniaapiserver.rest_skeleton.controller.base.AbstractRestController;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IPlatformService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "platforms")
public class PlatformRestController extends AbstractRestController<Platform,String> {

    @Autowired
    private IPlatformService platformService;

    private final static String root = "platforms";
    private final static String primaryKey = "acronym";

    @Override
    public IAbstractService<Platform, String> getService() {
        return platformService;
    }
}