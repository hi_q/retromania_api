package com.hiq.retromaniaapiserver.rest_skeleton.service;

import com.hiq.retromaniaapiserver.model.Videogame;
import com.hiq.retromaniaapiserver.rest_skeleton.dao.VideogameRepository;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.AbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IVideogameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideogameService extends AbstractService<Videogame,String> implements IVideogameService {

    @Autowired
    protected VideogameRepository repository;

    @Override
    public VideogameRepository getRepository() {
        return repository;
    }
    public void setRepository(VideogameRepository repository) {
        this.repository = repository;
    }

    @Override
    public void update(String primaryKey, Videogame entity) {
        getRepository().save(
                new Videogame(
                        primaryKey,
                        entity.getName(),
                        entity.getSummary(),
                        entity.getPublisher(),
                        entity.getYear(),
                        entity.getPrice()));
    }

    @Override
    public Videogame get(String key) {
        System.out.println(key);
        return getRepository().findById(key).get();
    }

    @Override
    public List<Videogame> getVideogameByName(String name) {
        return getRepository().findByName(name);
    }
    @Override
    public List<Videogame> getVideogameByPublisher(String publisher) {
        return getRepository().findByPublisher(publisher);
    }
}
