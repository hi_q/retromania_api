/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.controller;

import com.hiq.retromaniaapiserver.model.Videogame;
import com.hiq.retromaniaapiserver.rest_skeleton.controller.base.AbstractRestController;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces.IVideogameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "videogames")
public class VideogameRestController extends AbstractRestController<Videogame,String> {

    @Autowired
    private IVideogameService videogameService;

    private final static String root = "videogames";
    private final static String primaryKey = "id";
    private final static String nameKey = "name";
    private final static String publisherKey = "publisher";

    @Override
    public IAbstractService<Videogame, String> getService() {
        return videogameService;
    }

    @RequestMapping(value = "/"+ nameKey +"/{"+ nameKey +"}", method = RequestMethod.GET)
    public List<Videogame> getByName(@PathVariable(nameKey) String name) {
        return videogameService.getVideogameByName(name);
    }
    @RequestMapping(value = "/"+ publisherKey +"/{"+ publisherKey +"}", method = RequestMethod.GET)
    public List<Videogame> getByCompany(@PathVariable(publisherKey) String publisher) {
        return videogameService.getVideogameByPublisher(publisher);
    }

}