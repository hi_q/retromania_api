/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiserver.rest_skeleton.service.base;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public abstract class AbstractService<E,PK extends Serializable> implements IAbstractService<E,PK> {

    public abstract CrudRepository<E,PK> getRepository();

    @Override
    public List<E> getAll() {
        return (List<E>) getRepository().findAll();
    }

    @Override
    public void add(E entity) {
        getRepository().save(entity);
    }
    
    @Override
    public abstract E get(PK primaryKey);

    /*
     *  Nota luciano: update ha de ser implementat per cada repositori separat
     */
    @Override
    public abstract void update(PK primaryKey, E entity);

    @Override
    public void remove(E entity) {
        getRepository().delete(entity);
    }

    @Override
    public long count() {
        return getRepository().count();
    }
}
