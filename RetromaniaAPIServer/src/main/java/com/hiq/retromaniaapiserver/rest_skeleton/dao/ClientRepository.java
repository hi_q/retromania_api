package com.hiq.retromaniaapiserver.rest_skeleton.dao;

import com.hiq.retromaniaapiserver.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ClientRepository extends CrudRepository<Client, String> {
    @Override
    List<Client> findAll();
    Client findByNIE(String id);
    Optional<Client> findById(String id);
    List<Client> findByUsername(String username);
}
