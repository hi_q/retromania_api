package com.hiq.retromaniaapiserver.rest_skeleton.service.ifaces;

import com.hiq.retromaniaapiserver.model.Client;
import com.hiq.retromaniaapiserver.rest_skeleton.service.base.IAbstractService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IClientService extends IAbstractService<Client,String> {
    public List<Client> getClientByUsername(String username);
}
