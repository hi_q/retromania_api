package com.hiq.retromaniaapiclient.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;

@JsonRootName("videogameCopy")
@JacksonXmlRootElement(localName = "videogameCopy")
public class VideogameCopy {
    @JsonProperty("internalID")
    @JacksonXmlProperty(isAttribute = true,localName = "internalID")
    private String internalID;
    @JsonIgnore
    private Videogame videogame;

    public VideogameCopy(){

    }
    public VideogameCopy(String internalID){
        this.internalID = internalID;
    }
    public VideogameCopy(String internalID, Videogame videogame){
        this.internalID = internalID;
        this.videogame = videogame;
    }

    public String getInternalID() {
        return internalID;
    }
    public void setInternalID(String internalID) {
        this.internalID = internalID;
    }

    public Videogame getVideogame() {
        return videogame;
    }
    public void setVideogame(Videogame videogame) {
        this.videogame = videogame;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(
                this.getClass().getSimpleName(),
                internalID
        ).toString();
    }
}
