package com.hiq.retromaniaapiclient.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@JsonPropertyOrder({"id","date","client","copies"})
@JsonRootName("sale")
@JacksonXmlRootElement(localName = "sale")
public class Sale {
    @JsonProperty("id")
    @JacksonXmlProperty(isAttribute = true,localName = "id")
    private String id;
    private Date date;
    @JsonProperty("address")
    @JacksonXmlProperty(localName = "address")
    private String address;
    @JsonProperty("client")
    @JacksonXmlProperty(localName = "client")
    private Client client;
    @JsonProperty("copies")
    @JacksonXmlProperty(localName = "copies")
    private ArrayList<VideogameCopy> videogameCopies;

    protected Sale() {
    }
    public Sale(String id, Date date, String address, Client client, ArrayList<VideogameCopy> videogameCopies) {
        this.id = id;
        this.date = date;
        this.address = address;
        this.client = client;
        this.videogameCopies = videogameCopies;
    }

    @JsonIgnore
    private SimpleDateFormat defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat getDefaultDateFormat(){
        return defaultDateFormat;
    }
    private void setDefaultDateFormat(SimpleDateFormat simpleDateFormat){
        this.defaultDateFormat = simpleDateFormat;
    }

    @JsonIgnore
    public Date getDate() {
        return date;
    }
    @JsonProperty(value = "date")
    @JacksonXmlProperty(localName = "date")
    public String getFormattedDate(){
        return getDefaultDateFormat().format(getDate());
    }
    @JsonIgnore
    public void setDate(Date date) {
        this.date = date;
    }
    public void setFormattedDate(String date) throws ParseException {
        this.date = getDefaultDateFormat().parse(date);
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public ArrayList<VideogameCopy> getVideogameCopies() {
        return videogameCopies;
    }
    public void setVideogameCopies(ArrayList<VideogameCopy> videogameCopies) {
        this.videogameCopies = videogameCopies;
    }

    @Override
    public String toString() {
        String clientString =
                client.toString().replace("\n","\n\t");
        StringBuilder copiesStringBuild = new StringBuilder("VideogameCopies:\n\t\t");
        for (VideogameCopy copy:
                videogameCopies) {
            copiesStringBuild.append(
                    copy.toString().replace("\n","\n\t\t"));
            copiesStringBuild.append("\n\t\t");
        }
        String copiesString = copiesStringBuild.toString();
        String dateString =
                getFormattedDate();

        return new ToStringBuilder(
                getClass().getSimpleName(),
                id,
                dateString,
                address,
                clientString,
                copiesString
        ).toString();
    }
}
