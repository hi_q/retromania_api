package com.hiq.retromaniaapiclient.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;

@JsonRootName("client")
@JacksonXmlRootElement(localName = "client")
public class Client {
    @JsonProperty("key")
    @JacksonXmlProperty(localName = "key",isAttribute = true)
    private String key;
    @JsonProperty("nie")
    @JacksonXmlProperty(localName = "nie")
    private String NIE;
    @JsonProperty("username")
    @JacksonXmlProperty(localName = "username")
    private String username;
    @JsonProperty("password")
    @JacksonXmlProperty(localName = "password")
    private String password;
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @JsonProperty("surname")
    @JacksonXmlProperty(localName = "surname")
    private String surname;
    @JsonProperty("phone")
    @JacksonXmlProperty(localName = "phone")
    private String phone;
    @JsonProperty("email")
    @JacksonXmlProperty(localName = "email")
    private String email;

    public Client() {
    }
    public Client(String key, String NIE, String username, String password) {
        this.key = key;
        this.NIE = NIE;
        this.username = username;
        this.password = password;
    }
    public Client(String key, String NIE, String username, String password, String name, String surname, String phone, String email) {
        this.key = key;
        this.NIE = NIE;
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    @JsonIgnore
    public String getNIE() {
        return NIE;
    }
    public void setNIE(String NIE) {
        this.NIE = NIE;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(getClass().getSimpleName(),
                key,
                NIE,
                username,
                password,
                name,
                surname,
                phone,
                email)

                .toString();
    }
}
