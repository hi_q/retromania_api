/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;

import java.io.Serializable;

/**
 *
 * @author Luciano
 */
@JsonRootName("platform")
@JacksonXmlRootElement(localName = "platform")
public class Platform implements Serializable {
    @JsonProperty("acronym")
    @JacksonXmlProperty(localName = "acronym")
    private String acronym;
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @JsonProperty("company")
    @JacksonXmlProperty(localName = "company")
    private String company;

    protected Platform() {
    }
    public Platform(String acronym, String name, String company) {
        this.acronym = acronym;
        this.name = name;
        this.company = company;
    }

    public String getAcronym() {
        return acronym;
    }
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder("Platform",acronym,name);
        return builder.toString();
    }

}
