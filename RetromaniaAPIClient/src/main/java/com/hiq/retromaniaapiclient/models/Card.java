/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.models;

/**
 *
 * @author Michael
 */
public class Card {

    String name;
    Double ports;

    public Card() {
    }

    public Card(String name, Double ports) {
        this.name = name;
        this.ports = ports;
    }

    public String getName() {
        return name;
    }

    public Double getPorts() {
        return ports;
    }

    @Override
    public String toString() {
        return name;
    }

}
