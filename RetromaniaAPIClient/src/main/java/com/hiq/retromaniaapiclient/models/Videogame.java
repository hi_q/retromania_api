package com.hiq.retromaniaapiclient.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;
import com.hiq.retromaniaapiclient.models.utils.ToStringBuilder;
import java.util.ArrayList;

@JsonRootName("videogame")
@JacksonXmlRootElement(localName = "videogame")
public class Videogame {
    @JsonProperty("id")
    @JacksonXmlProperty(isAttribute = true,localName = "id")
    private String ID;
    @JsonProperty("name")
    @JacksonXmlProperty(localName = "name")
    private String name;
    @JsonProperty("summary")
    @JacksonXmlProperty(localName = "summary")
    private String summary;
    @JsonProperty("publisher")
    @JacksonXmlProperty(localName = "publisher")
    private String publisher;
    @JsonProperty("year")
    @JacksonXmlProperty(localName = "year")
    private Short year;
    @JsonProperty("price")
    @JacksonXmlProperty(localName = "price")
    private Double price;

    @JsonProperty("platform")
    @JacksonXmlProperty(localName = "platform")
    private Platform platform;

    @JsonProperty("copies")
    @JacksonXmlProperty(localName = "copies")
    private ArrayList<VideogameCopy> copies;

    protected Videogame() {

    }
    public Videogame(String ID, String name, String summary, String publisher, Short year, Double price) {
        this.ID = ID;
        this.name = name;
        this.summary = summary;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
    }
    public Videogame(String ID, String name, String summary, String publisher, Short year, Double price, Platform platform, ArrayList<VideogameCopy> copies) {
        this.ID = ID;
        this.name = name;
        this.summary = summary;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
        this.platform = platform;
        this.copies = copies;
    }

    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public String getPublisher() {
        return publisher;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public Short getYear() {
        return year;
    }
    public void setYear(Short year) {
        this.year = year;
    }
    public Platform getPlatform() {
        return platform;
    }
    public void setPlatform(Platform platform) {
        this.platform = platform;
    }
    public ArrayList<VideogameCopy> getCopies() {
        return copies;
    }
    public void setCopies(ArrayList<VideogameCopy> copies) {
        this.copies = copies;
    }

    @Override
    public String toString() {
        String platformString =
                platform.toString().replace("\n","\n\t");
        StringBuilder copiesStringBuild = new StringBuilder("VideogameCopies:\n\t\t");
        if (copies == null){
            copiesStringBuild.setLength(0);
            copiesStringBuild.append("No copies");
        }
        else for (VideogameCopy copy:
             copies) {
            copiesStringBuild.append(
                    copy.toString().replace("\n","\n\t\t"));
            copiesStringBuild.append("\n\t\t");
        }
        String copiesString = copiesStringBuild.toString();


        return new ToStringBuilder
                (getClass().getSimpleName(),
            ID,
                    name,
                    summary,
                    publisher,
                    year.toString(),
                        platformString,
                        copiesString)

                .toString();
    }
}
