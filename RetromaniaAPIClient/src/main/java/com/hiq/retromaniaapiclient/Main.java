/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient;

import com.hiq.retromaniaapiclient.controller.DetailsController;
import com.hiq.retromaniaapiclient.controller.JoinController;
import com.hiq.retromaniaapiclient.controller.LoginController;
import com.hiq.retromaniaapiclient.controller.SaleController;
import com.hiq.retromaniaapiclient.controller.UserController;
import com.hiq.retromaniaapiclient.models.Client;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hiq.retromaniaapiclient.models.Videogame;
import com.hiq.retromaniaapiclient.models.utils.JacksonConverter;
import com.hiq.retromaniaapiclient.transactions.ApiRequestOperation;
import com.hiq.retromaniaapiclient.transactions.GenericApiRequest;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Michael
 */
@SpringBootApplication
public class Main extends Application implements CommandLineRunner{

    private static Stage stage;
    private static LoginController loginController;
    public static LoginController getLoginController(){
        return loginController;
    }
    private static DetailsController detailsController;
    public static DetailsController getDetailsController() {
        return detailsController;
    }
    private static UserController userController;
    public static UserController getUserController() {
        return userController;
    }

    @Override
    public void start(Stage stage) throws Exception {
        Main.stage = stage;
        System.out.println(new File(".").getCanonicalPath());
        stage.getIcons().add(new Image("file:src/main/java/com/hiq/retromaniaapiclient/images/icon.png"));
        startLoginStage();
    }

    public static void startLoginStage() throws IOException {
        FXMLLoader view = new FXMLLoader(new File("src/main/java/com/hiq/retromaniaapiclient/view/LoginView.fxml").toURL());
        loginController = new LoginController();
        view.setController(loginController);

        Parent root = view.load();
        Scene scene = new Scene(root);
        stage.setTitle("retromaniaapiclient");

        stage.setScene(scene);
        stage.show();
    }

    public static void startJoinStage() throws IOException {
        FXMLLoader view = new FXMLLoader(new File("src/main/java/com/hiq/retromaniaapiclient/view/JoinView.fxml").toURL());

        JoinController controller = new JoinController();

        view.setController(controller);

        Parent root = view.load();
        Scene scene = new Scene(root);
        stage.setTitle("retromaniaapiclient");
        stage.setScene(scene);
        stage.show();
    }

    public static void startUserStage() throws IOException {
        FXMLLoader view = new FXMLLoader(new File("src/main/java/com/hiq/retromaniaapiclient/view/UserView.fxml").toURL());

        userController = new UserController();
        view.setController(userController);

        Parent root = view.load();
        Scene scene = new Scene(root);
        stage.setTitle("retromaniaapiclient");
        stage.setScene(scene);
        stage.show();
    }

    public static void startDetailsStage(Videogame actualgame) throws IOException {
        FXMLLoader view = new FXMLLoader(new File("src/main/java/com/hiq/retromaniaapiclient/view/DetailsView.fxml").toURL());

        detailsController = new DetailsController();
        detailsController.setActualGame(actualgame);

        view.setController(detailsController);

        Parent root = view.load();
        Scene scene = new Scene(root);
        stage.setTitle("retromaniaapiclient");
        stage.setScene(scene);
        stage.show();
    }

    public static void startSaleStage(String game, double cost) throws IOException {
        FXMLLoader view = new FXMLLoader(new File("src/main/java/com/hiq/retromaniaapiclient/view/SaleView.fxml").toURL());

        SaleController controller = new SaleController();

        controller.setGame(game);
        controller.setCost(cost);

        view.setController(controller);

        Parent root = view.load();
        Scene scene = new Scene(root);
        stage.setTitle("retromaniaapiclient");
        stage.setScene(scene);
        stage.show();
    }

    public static void cleanStage() {
        stage.hide();
        stage.close();
    }

    
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        GenericApiRequest<Videogame> request = new ApiRequestOperation<Videogame>
                (Videogame.class,
                "videogames");

        Videogame juego = request.get("SLES_54321");

        JacksonConverter<Videogame> convertidor = new JacksonConverter<>(Videogame.class);
        convertidor.toXmlFile(juego,"juego.xml");


        launch(args);
    }
    

    
}
