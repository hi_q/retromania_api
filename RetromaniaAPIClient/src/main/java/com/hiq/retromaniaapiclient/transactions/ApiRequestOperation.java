package com.hiq.retromaniaapiclient.transactions;

import com.hiq.retromaniaapiclient.models.Videogame;
import com.sun.net.httpserver.HttpServer;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ApiRequestOperation<E> implements GenericApiRequest<E>{
    Class<E> type;
    String root;
    public ApiRequestOperation(Class<E> type, String root){
        this.type = type;
        this.root = root;
    }

    private final static String requestBase = "http://localhost:8080/";
                            private final static String requestSuffix = "%s/%s";

    public List<E> getAll(){
        try{
        return new RestTemplate().exchange(
                requestBase+ String.format(requestSuffix, root,""),
                HttpMethod.GET,
                null,
                new SpecialArrayType())
            .getBody();
        }
            catch(HttpServerErrorException error){
            System.err.println(error);
            return null;
        }
    }

    public E get(String primaryKey){
        try{
        return new RestTemplate().exchange(
                requestBase+ String.format(requestSuffix, root,primaryKey),
                HttpMethod.GET,
                null,
                type)
            .getBody();

        }
        catch(HttpServerErrorException error){
            System.err.println(error);
            return null;
        }
    }
    public List<E> getAsList(String primaryKey){
        try{
            return new RestTemplate().exchange(
                    requestBase+ String.format(requestSuffix, root,primaryKey),
                    HttpMethod.GET,
                    null,
                    new SpecialArrayType())
                    .getBody();

        }
        catch(HttpServerErrorException error){
            System.err.println(error);
            return null;
        }
    }


    public boolean insert(E entity){
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<E> requestEntity = new HttpEntity<E>(entity, headers);

        ResponseEntity<Boolean> result = new RestTemplate().exchange(
                requestBase+ String.format(requestSuffix,root,""),
                HttpMethod.POST,
                requestEntity,
                Boolean.class);

        return result.getBody();
        }
        catch(HttpServerErrorException error){
            System.err.println(error);
            return false;
        }
    }
    @Override
    public void edit(E entity){
        try{
        new RestTemplate().put(
                requestBase+ String.format(requestSuffix, root,""),
                entity,
                type);
        }
        catch(HttpServerErrorException error){
            System.err.println(error);
        }
    }

    @Override
    public void delete(String primaryKey){
        try{
        new RestTemplate().delete(
                requestBase+ String.format(requestSuffix, root,""),
                primaryKey);
        }
        catch(HttpServerErrorException error){
            System.err.println(error);
        }
    }

    public Long count(){
        try{
        return new RestTemplate().exchange(
                requestBase+ String.format(requestSuffix, root,""),
                HttpMethod.GET,
                null,
                Long.class)
                .getBody();
        }
            catch(HttpServerErrorException error){
            System.err.println(error);
            return null;
        }
    }

    private class SpecialArrayType extends ParameterizedTypeReference<ArrayList<E>> {
        @Override
        public Type getType() {
            return new ParameterizedType() {
                @Override
                public Type getRawType() {
                    return List.class;
                }

                @Override
                public Type getOwnerType() {
                    return null;
                }

                @Override
                public Type[] getActualTypeArguments() {
                    return new Type[]{type};
                }
            };
        }};
}
