package com.hiq.retromaniaapiclient.transactions;

import java.util.List;

public interface GenericApiRequest<E> {
    public List<E> getAll();
    public E get(String primaryKey);
    public boolean insert(E entity);
    public void edit(E entity);
    public void delete(String primaryKey);
    public Long count();
}
