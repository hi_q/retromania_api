/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.controller;

import com.hiq.retromaniaapiclient.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hiq.retromaniaapiclient.controller.utils.HashAlgorithm;
import com.hiq.retromaniaapiclient.controller.utils.HashProcess;
import com.hiq.retromaniaapiclient.models.Client;
import com.hiq.retromaniaapiclient.transactions.ApiRequestOperation;
import com.hiq.retromaniaapiclient.transactions.GenericApiRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Michael luciano
 */
public class LoginController implements Initializable {

    private Client client;
    public Client getClient(){
        return client;
    }
    public void setClient(Client client){
        this.client = client;
    }
    
    @FXML
    private AnchorPane loginAnchorPane;
    @FXML
    private TextArea loginTextArea;
    @FXML
    private TextField loginTextFieldUser;
    @FXML
    private PasswordField loginPasswordFieldPass;
    @FXML
    private Button loginButtonJoin;
    @FXML
    private Button loginButtonGo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.client = null;
        initTextArea();
        initButtonJoin();
        initButtonGetIn();
        loginTextFieldUser.clear();
        loginPasswordFieldPass.clear();
    }

    public void initTextArea() {
        loginTextArea.setText("¡Bienvenidos a retromanía!\n\n"
                + "NOVEDADES:\n"
                + "Añadidos adicionales para filtrar\n"
                + "juegos en las búsquedas.\n\n"
                + "¡Descuentos de verano en curso,\n"
                + "no te los pierdas!\n\n\n\n\n\n\n"
                + "Retromanía es un negocio de\n"
                + "venta de juegos retro online.\n"
                + "Creado por Luciano Castro\n"
                + "y Michael Castaño.");
    }

    public void initButtonJoin() {
        loginButtonJoin.setOnAction(this::join);
    }

    public void initButtonGetIn() {
        loginButtonGo.setOnAction(this::getIn);
    }

    private void getIn(ActionEvent event) {
        String expectedKey = getKey();
        Client result = getClient(expectedKey);
        try {
            if (result != null) {
                this.client = result;
                Main.cleanStage();
                Main.startUserStage();
            } else {
                this.client = null;
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Inicio de sesión");
                alert.setHeaderText("No se ha podido iniciar sesión:");
                alert.setContentText("Usuario o contraseña no encontrada en la base de datos!");
                alert.showAndWait();
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getKey() {
        String userInserted = loginTextFieldUser.getText();
        String passInserted = loginPasswordFieldPass.getText();

        HashAlgorithm algoritmo = HashAlgorithm.SHA256;

        String userPass = userInserted+passInserted;
        HashProcess hashGen = new HashProcess(userPass, algoritmo);
        hashGen.process();

        return hashGen.getMessageProcessedAsString();
    }

    private void join(ActionEvent event) {
        try {
            Main.cleanStage();
            Main.startJoinStage();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Client getClient(String key){
        GenericApiRequest<Client> request = new ApiRequestOperation<>(Client.class,"clients");
        Client result = request.get(key);
        return result;
    }
    
}
