package com.hiq.retromaniaapiclient.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.hiq.retromaniaapiclient.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hiq.retromaniaapiclient.models.Videogame;
import com.hiq.retromaniaapiclient.transactions.ApiRequestOperation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Michael && Luciano
 */
public class UserController implements Initializable { 

    
    @FXML
    private AnchorPane userAnchorPane;
    @FXML
    private ComboBox userComboBox;
    @FXML
    private ListView userListView;
    @FXML
    private TextField userTextField;
    @FXML
    private Button UserButtonDisconnect;
    @FXML
    private Button userButtonSearch;
    @FXML
    private Button userButtonShow;
    @FXML
    private Button userButtonSale;
    @FXML
    private Label userLabel;
    @FXML
    private Label userLabelName;
    @FXML
    private Label userLabelPublisher;
    @FXML
    private Label userLabelYear;
    @FXML
    private Label userLabelPlatform;
    @FXML
    private Label userLabelPrice;

    private Videogame actualGame; //copia del juego seleccionado en la lista
    ObservableList<Videogame> gameList = FXCollections.observableArrayList();
    ObservableList<String> searchoptions = FXCollections.observableArrayList();
    int comboselected = -1; //si no hay nada en el combo, es -1.

    
    @FXML
    private void Search(ActionEvent event) {
        if (comboselected != -1) {
            String busqueda = userTextField.getText();
            userTextField.clear();
            //Usar "busqueda" y "searchoptions" para hacer las consultas y rellenar el combo y el listview. 
        } else {
            List<Videogame> items = userListView.getItems();
            items.clear();
            items.addAll(new ApiRequestOperation<Videogame>(Videogame.class,"videogames").getAll());
            switch((String)userComboBox.getSelectionModel().getSelectedItem()){
                case "ID":
                    for (int i = 0; i < items.size(); i++)
                        if (!items.get(i).getID().equalsIgnoreCase(userTextField.getText()))
                            items.remove(i);
                    break;
                case "Name":
                    for (int i = 0; i < items.size(); i++)
                        if (!items.get(i).getName().startsWith(userTextField.getText()))
                            items.remove(i);
                    break;
                case "Publisher":
                    for (int i = 0; i < items.size(); i++)
                        if (!items.get(i).getPublisher().startsWith(userTextField.getText()))
                            items.remove(i);
                    break;
            }
            cleanInfo();
        }
        //gameList.add(actualGame); // Comentado porque peta por tu "ToString" de videogame, pero la idea 
        //userListView.setItems(gameList); //es esta; añadirlo a la lista.
    }

    @FXML
    private void changeSearch() {
        userTextField.clear();
        userListView.getItems().clear();
        userListView.getItems().addAll(
                new ApiRequestOperation<Videogame>(Videogame.class,"videogames").getAll()
        );
        cleanInfo();
    }

    @Override 
    public void initialize(URL url, ResourceBundle rb) {
        initButtonBack();
        initButtonDetails();
        initButtonSale();
        searchoptions.add("ID"); //aquí tu añades lo que quieras.
        searchoptions.add("Name");
        searchoptions.add("Publisher");
        userComboBox.setItems(searchoptions);
        String usuario = Main.getLoginController().getClient().getName();
        userLabel.setText("Bienvenid@ " + usuario);
        userListView.setCellFactory(param -> new ListCell<Videogame>() {
            @Override
            protected void updateItem(Videogame item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                } else {
                    setText(item.getID()+" - "+item.getName());
                }
            }
        });
        userListView.getItems().addAll(
                new ApiRequestOperation<Videogame>(Videogame.class,"videogames").getAll()
        );
        userListView.setOnMouseClicked(this::fillInfo);
        userComboBox.getSelectionModel().selectFirst();
    }

    public void fillInfo(MouseEvent event){
        Object item = userListView.getSelectionModel().getSelectedItem();
        if (item != null) {
            Videogame game = (Videogame) item;
            actualGame = game;
            userLabelName.setText(game.getName());
            userLabelPublisher.setText(game.getPublisher());
            userLabelPlatform.setText(game.getPlatform().getName());
            userLabelPrice.setText(game.getPrice().toString());
            userLabelYear.setText(game.getYear().toString());
        }
    }
    public void cleanInfo(){
        actualGame = null;
        userLabelName.setText("--------------------");
        userLabelPublisher.setText("--------------------");
        userLabelPlatform.setText("--------------------");
        userLabelPrice.setText("--------------------");
        userLabelYear.setText("--------------------");

    }

    public void initButtonBack() {
        UserButtonDisconnect.setOnAction(this::back);
    }

    public void initButtonDetails() {
        userButtonShow.setOnAction(this::details);
    }

    public void initButtonSale() {
        userButtonSale.setOnAction(this::sale);
    }

    private void back(ActionEvent event) {
        try {
            Main.cleanStage();
            Main.startLoginStage();
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void details(ActionEvent event) {
        try {
            if (actualGame == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Selección de juego");
                alert.setHeaderText("No se han podido mostrar los detalles.");
                alert.setContentText("No hay ningún juego seleccionado.");
                alert.showAndWait();
            }
            else{
                Main.cleanStage();
                Main.startDetailsStage(actualGame);
            }
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sale(ActionEvent event) {
        try {
            if (actualGame == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Selección de juego");
                alert.setHeaderText("No se ha podido comprar.");
                alert.setContentText("No hay ningún juego seleccionado.");
                alert.showAndWait();
            }
            else{

                String game = actualGame.getName();
                double cost = actualGame.getPrice();
                Main.cleanStage();
                Main.startSaleStage(game, cost);
            }
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Videogame getActualGame() {
        return actualGame;
    }
}
