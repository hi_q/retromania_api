/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.controller;

import com.hiq.retromaniaapiclient.Main;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hiq.retromaniaapiclient.controller.utils.HashAlgorithm;
import com.hiq.retromaniaapiclient.controller.utils.HashProcess;
import com.hiq.retromaniaapiclient.models.Client;
import com.hiq.retromaniaapiclient.transactions.ApiRequestOperation;
import com.hiq.retromaniaapiclient.transactions.GenericApiRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Michael y luciano
 */
public class JoinController implements Initializable {

    @FXML
    private AnchorPane joinAnchorPane;
    @FXML
    private TextField joinTextFieldUser;
    @FXML
    private PasswordField joinPasswordFieldPass;
    @FXML
    private PasswordField joinPasswordFieldPassConfirmed;
    @FXML
    private TextField joinTextFieldNif;
    @FXML
    private TextField joinTextFieldEmail;
    @FXML
    private TextField joinTextFieldName;
    @FXML
    private TextField joinTextFieldSubname;
    @FXML
    private TextField joinTextFieldPhone;
    @FXML
    private Button joinButtonGo;
    @FXML
    private Button joinButtonBack;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initButtonJoin();
        initButtonBack();
    }

    public void initButtonJoin() {
        joinButtonGo.setOnAction(this::join);
    }

    public void initButtonBack() {
        joinButtonBack.setOnAction(this::back);
    }

    private void join(ActionEvent event) {
        try {
            if (checkUser() && checkPass() && checkNif() && checkName()
                    && checkSubname() && checkPhone() && checkEmail()) {



                ApiRequestOperation<Client> clientApiRequestOperation = new ApiRequestOperation<Client>(Client.class,"clients");
                List<Client> res;
                if ((res = clientApiRequestOperation.getAsList("username/"+joinTextFieldUser.getText())).size() > 0 /*llista no buida - error - s'ha trobat*/){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Creación de usuario");
                    alert.setHeaderText("No se ha podido crear la cuenta:");
                    alert.setContentText("El usuario "+joinTextFieldUser.getText()+" ya existe.");
                    alert.showAndWait();
                }
                else{
                    String key = getKey();
                    Client newClient = new Client(
                            //(String key, String NIE, String username, String password, String name, String surname, String phone, String email)
                            key,
                            joinTextFieldNif.getText(),
                            joinTextFieldUser.getText(),
                            joinPasswordFieldPass.getText(),
                            joinTextFieldName.getText(),
                            joinTextFieldSubname.getText(),
                            joinTextFieldPhone.getText(),
                            joinTextFieldEmail.getText()
                    );

                    if (clientApiRequestOperation.insert(newClient)){
                        Main.cleanStage();
                        Main.startLoginStage();
                    }
                    else{
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Creación de usuario");
                        alert.setHeaderText("No se ha podido crear la cuenta:");
                        alert.setContentText("Se ha producido algun error. Por favor, contacta con el administrador..");
                        alert.showAndWait();
                    }
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Creación de usuario");
                alert.setHeaderText("No se ha podido crear la cuenta:");
                alert.setContentText("Campos incorrectos.");
                alert.showAndWait();
            }
        } catch (IOException ex) {
            Logger.getLogger(JoinController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void back(ActionEvent event) {
        try {
            Main.cleanStage();
            Main.startLoginStage();
        } catch (IOException ex) {
            Logger.getLogger(JoinController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean checkUser() {

        if (!joinTextFieldUser.getText().isEmpty()) {
            if (true) {//Si el usuario está disponible en la BD (no coincidencias)   
                return true;
            } else {
                joinTextFieldUser.clear();
                return false;
            }
        } else {
            joinTextFieldUser.clear();
            return false;
        }
    }

    private boolean checkPass() {
        if (!joinPasswordFieldPass.getText().isEmpty() && !joinPasswordFieldPassConfirmed.getText().isEmpty()
                && joinPasswordFieldPass.getText().equals(joinPasswordFieldPassConfirmed.getText())) {
            if (joinPasswordFieldPass.getText().equals(joinPasswordFieldPassConfirmed.getText())){
                return true;
            }
            else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Creación de usuario");
                alert.setHeaderText("No se ha podido crear la cuenta:");
                alert.setContentText("Las contraseñas no coinciden.");
                alert.showAndWait();
                return false;
            }

        } else {
            joinPasswordFieldPass.clear();
            joinPasswordFieldPassConfirmed.clear();
            return false;
        }
    }

    private boolean checkNif() { //Comprueba si tiene los 8 dígitos y un letra al final
        char[] nif = joinTextFieldNif.getText().toCharArray();
        int letra = (nif.length - 1);

        if (nif.length == 9) {
            if ((nif[letra] >= 65 && nif[letra] <= 90) || (nif[letra] >= 97 && nif[letra] <= 122)) {
                return true;
            } else {
                joinTextFieldNif.clear();
                return false;
            }
        } else {
            joinTextFieldNif.clear();
            return false;
        }
    }

    private boolean checkName() {//simplemente si no está vacío
        if (!joinTextFieldName.getText().isEmpty()) {
            return true;
        } else {
            joinTextFieldName.clear();
            return false;
        }
    }

    private boolean checkSubname() {//simplemente si no está vacío
        if (!joinTextFieldSubname.getText().isEmpty()) {
            return true;
        } else {
            joinTextFieldSubname.clear();
            return false;
        }
    }

    private boolean checkPhone() {//Si no está vacio y si todo es int
        boolean comprobador = true;
        char[] numero = joinTextFieldPhone.getText().toCharArray();
        int ciclo = 0;
        while (ciclo < numero.length) {
            if (numero[ciclo] < 48 || numero[ciclo] > 57) {
                comprobador = false;
            } else {
            }
            ciclo++;
        }
        if (comprobador == true && !joinTextFieldPhone.getText().isEmpty()) {
            return true;
        } else {
            joinTextFieldPhone.clear();
            return false;
        }
    }

    private boolean checkEmail() {//simplemente si no está vacío
        if (!joinTextFieldEmail.getText().isEmpty()) {
            return true;
        } else {
            joinTextFieldEmail.clear();
            return false;
        }
    }
    public String getKey() {
        String userInserted = joinTextFieldUser.getText();
        String passInserted = joinPasswordFieldPass.getText();

        HashAlgorithm algoritmo = HashAlgorithm.SHA256;

        String userPass = userInserted+passInserted;
        HashProcess hashGen = new HashProcess(userPass, algoritmo);
        hashGen.process();

        return hashGen.getMessageProcessedAsString();
    }
    public Client getClient(String key){
        GenericApiRequest<Client> request = new ApiRequestOperation<>(Client.class,"clients");
        Client result = request.get(key);
        return result;
    }

}
