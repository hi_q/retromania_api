/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.controller;

import com.hiq.retromaniaapiclient.Main;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hiq.retromaniaapiclient.models.Card;
import com.hiq.retromaniaapiclient.models.Sale;
import com.hiq.retromaniaapiclient.models.VideogameCopy;
import com.hiq.retromaniaapiclient.transactions.ApiRequestOperation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import javax.print.attribute.standard.Copies;

/**
 *
 * @author Michael luciano
 */
public class SaleController implements Initializable {

    @FXML
    private AnchorPane saleAnchorPane;

    @FXML
    private Button saleButtonBack;
    @FXML
    private Button saleButtonGo;
    @FXML
    private Label saleLabelGame;
    @FXML
    private Label saleLabelCost;
    @FXML
    private Label saleLabelTotal;
    @FXML
    private ComboBox saleComboBoxPaymentMethod;
    @FXML
    private TextField saleTextFieldAddress;

    private String game;
    private Double cost;
    private Double ports;
    int comboselected = -1; //si no hay nada en el combo, es -1.
    
    ObservableList<Card> cardslist = FXCollections.observableArrayList();

    @FXML
    private void buy(ActionEvent event) throws IOException {
        Alert alert;
        if (!saleTextFieldAddress.getText().isEmpty() && comboselected != -1) {

            ApiRequestOperation<Sale> apiRequestOperation= new ApiRequestOperation<>(Sale.class,"sales");

            // VENTA CODI
            List<Sale> currentSales = apiRequestOperation.getAll();
            String lastCode = currentSales.get(0).getId();
            for (int i = 1; i < currentSales.size(); i++) {
                if (currentSales.get(i).getId().compareTo(lastCode) > 0) lastCode = currentSales.get(i).getId();
            }
            Integer codeNumber = Integer.parseInt(lastCode.substring(2));
            if (Main.getUserController().getActualGame().getCopies() != null){
            ArrayList<VideogameCopy> copies = new ArrayList<>();
            copies.add(Main.getUserController().getActualGame().getCopies().get(0));
            Sale sale = new Sale(
                    String.format("S_%03d", codeNumber+1),
                    new Date(System.currentTimeMillis()),
                            saleTextFieldAddress.getText(),
                            Main.getLoginController().getClient(),
                            copies);

                if (apiRequestOperation.insert(sale)){
                    alert = new Alert(Alert.AlertType.INFORMATION); //Si hay tarjeta seleccionada y calle puesta
                    alert.setTitle("Compra exitosa");
                    alert.setContentText("Gracias por su compra. \n"
                            + "En breve recibirá un  correo con toda la información.");
                    Main.cleanStage();
                    Main.startUserStage();
                }
                else{
                    alert = new Alert(Alert.AlertType.INFORMATION); //Si hay tarjeta seleccionada y calle puesta
                    alert.setTitle("Compra fallida");
                    alert.setContentText("No se ha podido realizar el pedido. \n"
                            + "Contacte con el administrador para más información.");
                    Main.cleanStage();
                    Main.startUserStage();
                    }
            }
            else{
                alert = new Alert(Alert.AlertType.ERROR); //Si hay tarjeta seleccionada y calle puesta
                alert.setTitle("Compra fallida");
                alert.setContentText("No ha podido comprarse el juego. \n"
                        + "No existen copias actualmente del juego.");
                Main.cleanStage();
                Main.startUserStage();
            }




        } else {
            alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error en la compra");
            alert.setHeaderText("No se ha podido realizar la compra:");
            if (saleTextFieldAddress.getText().isEmpty()) {
                alert.setContentText("Dirección no añadida!");
            } else {
                alert.setContentText("No hay método de pago seleccionado!");
            }
        }
        alert.showAndWait();
    }

    @FXML
    private void changepaymentMethods() {
        comboselected = saleComboBoxPaymentMethod.getSelectionModel().getSelectedIndex();
        saleButtonGo.setVisible(true);
        ports = cardslist.get(comboselected).getPorts();
        Double total = ((cost * ports) + cost); 
        saleLabelTotal.setText(total.toString());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initButtonBack();
        Card visa = new Card("Visa", 0.5);
        Card mastercard = new Card("Mastercard", 0.7);//info inventada. recuerda que las tarjetas son SIMBOLICAS
        saleLabelGame.setText(game);
        saleLabelCost.setText(cost.toString());
        saleButtonGo.setVisible(false);
        cardslist.add(visa);
        cardslist.add(mastercard);
        saleComboBoxPaymentMethod.setItems(cardslist); 
    }

    public void initButtonBack() {
        saleButtonBack.setOnAction(this::back);
    }

    private void back(ActionEvent event) {
        try {
            Main.cleanStage();
            Main.startUserStage();
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setGame(String game) {
        this.game = game;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

}
