/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiq.retromaniaapiclient.controller;

import com.hiq.retromaniaapiclient.Main;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hiq.retromaniaapiclient.models.Videogame;
import com.hiq.retromaniaapiclient.models.utils.JacksonConverter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Michael y luciano
 */
public class DetailsController implements Initializable {

    @FXML
    private AnchorPane detailsAnchorPane;
    @FXML
    private Button detailsButtonXML;
    @FXML
    private Button detailsButtonBack;
    @FXML
    private TextArea detailsTextAreaSummary;
    @FXML
    private Label detailsLabelID;
    @FXML
    private Label detailsLabelName;
    @FXML
    private Label detailsLabelPublisher;
    @FXML
    private Label detailsLabelYear;
    @FXML
    private Label detailsLabelPlatform;
    @FXML
    private Label detailsLabelPrice;

    Videogame actualGame;


    @FXML
    private void xml(ActionEvent event) throws IOException {
        //Aquí se ha de poner todo lo necesario para sacar el XML. 
        //Con el "actualGame" se puede obtener la info necesaria.
        JacksonConverter<Videogame> conversion = new JacksonConverter<>(Videogame.class);
        conversion.toXmlFile(actualGame,"output.xml");
        conversion.toJsonFile(actualGame,"output.json");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Guardado múltiple");
        alert.setHeaderText("Se ha guardado el archivo.");
        alert.setContentText("Archivo guardado en output.xml y output.json.");
        alert.showAndWait();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initButtonBack();
        detailsLabelID.setText(actualGame.getID());
        detailsLabelName.setText(actualGame.getName());
        detailsLabelPublisher.setText(actualGame.getPublisher());
        detailsLabelYear.setText(actualGame.getYear().toString());
        detailsLabelPrice.setText(actualGame.getPrice().toString());
        detailsLabelPlatform.setText(actualGame.getPlatform().getName());
        detailsTextAreaSummary.setText(actualGame.getSummary());
    }

    public void initButtonBack() {
        detailsButtonBack.setOnAction(this::back);
    }

    private void back(ActionEvent event) {
        try {
            Main.cleanStage();
            Main.startUserStage();
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Videogame getActualGame() {
        return actualGame;
    }

    public void setActualGame(Videogame actualgame) {
        this.actualGame = actualgame;
    }

}
